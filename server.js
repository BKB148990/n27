var bodyParser = require('body-parser')
const mysql = require('mysql')
const dbVerbindung = mysql.createConnection({
    host: "10.40.38.110",
    user: "n27user",
    password: "BKB123456!",
    database: "dbn27"
})

dbVerbindung.connect()

dbVerbindung.query("CREATE TABLE IF NOT EXISTS konto (idKonto INT AUTO_INCREMENT, zeitstempel TIMESTAMP, idKunde INT, PRIMARY KEY(idKonto), FOREIGN KEY(idKunde) REFERENCES kunde(idKunde))", (err,rows) =>{
    if (err){
        console.log(err)
    }else{
    console.log("Tabelle konto angelegt oder schon vorhanden.")}
})

dbVerbindung.query("CREATE TABLE IF NOT EXISTS kontobewegung (idKonto INT, zeitstempel TIMESTAMP, text VARCHAR(150), gegenkonto INT, betrag DECIMAL(15,2), PRIMARY KEY(idKonto, zeitstempel), FOREIGN KEY(idKonto) REFERENCES konto(idKonto))", (err,rows) =>{
    if (err){
        console.log(err)
    }else{
    console.log("Tabelle kontobewegung angelegt oder schon vorhanden.")}
})

const express = require('express')
var cookieParser = require('cookie-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())
app.use(express.static('public')) 
app.listen(3000, function() {
    console.log('Der Server ist erfolgreich gestartet und lauscht auf Port 3000.')
})

app.get('/',(req, res, next) => {
    
    let kundenID = req.cookies['istAngemeldetAls']
    
    if(req.cookies['istAngemeldetAls']){
        console.log("ist angemeldet: " + req.cookies['istAngemeldetAls'])
        res.render("index.ejs", {
            content: 'Willkommen, ' + kundenID           
        })
    }else{
        res.render('login.ejs', {
            content: 'Willkommen, ' + kundenID   
        })
    }  
})

app.get('/login',(req, res, next) => { 
    
    console.log("Der Cookie wird gelöscht")
    res.cookie('istAngemeldetAls', '')
    res.render('login.ejs', {  
    })
    }  
)

app.get('/ueberweisung',(req, res, next) => {
    
    let kundenID = req.cookies['istAngemeldetAls']
    
    if(req.cookies['istAngemeldetAls']){
        console.log("ist angemeldet: " + req.cookies['istAngemeldetAls'])
        res.render("ueberweisung.ejs", {          
        })
    }else{
        res.render('login.ejs', {
            content: 'Willkommen, ' + kundenID   
        })
    }  
})

app.post('/',(req, res, next) => {     
    
    const kundenID = req.body.kundenId
    const password = req.body.password

    dbVerbindung.query("SELECT * FROM kunde WHERE idKunde = '"+ kundenID + "'  AND kennwort = '" +password+ "' ; ", (err,rows) =>{
        if (err){
            console.log(err)
        }
        else{
            console.log(rows.length)

            if(rows[0]){
                console.log("Kunde bekannt: " + rows[0].vorname + " " + rows[0].nachname + " " + rows[0].mail)
                console.log("Der Cookie wird gesetzt:")
                res.cookie('istAngemeldetAls', kundenID)
                res.render('index.ejs', {
                    content: 'Willkommen, ' + rows[0].vorname    
                })                
            }else{
                console.log("Kunde unbekannt")
                console.log("Der Cookie wird gelöscht")
                res.cookie('istAngemeldetAls', '')
                res.render('login.ejs', {
                    content: 'Willkommen, ' + rows[0].vorname     
                })
            }
        }
    })
})

// Die Funktion istAngemeldet() prüft, ob das gegebene Kennwort richtig ist.
// Entsprechend gibt sie true oder false zurück.

function istAngemeldet(kundenId, password){   
    if(kundenId === "4711" && password === "123"){
        return true
    }  
    return false
}

//con.query("CREATE TABLE kontobewegung IF NOT EXISTS (idKonto INT [PRIMARY KEY], Zeit TIMESTAMP, Betrag DECIMAL, Info VARCHAR(100));", function (err, result) {
  //  if (err) throw err;
  //  console.log("Tabelle konto angelegt, bzw. bereits vorhanden.")
//})

